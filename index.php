<?PHP

include 'server/session.php';

?>


<html>
    <title>ADM Questionnaire</title>
<head>
    <script src="https://code.jquery.com/jquery-latest.js"></script>
 
    <script src="client/scripts/checkForm.js"></script>
    <link href="client/styles/main.css" rel="stylesheet" type="text/css"/>
 

</head>

<body>

    <div id="title" class="title" >
        <div class="show">
          Questionnaire
        </div>
    </div>   

    
        <div id="msg" class="hidden msg" >
          <br>Thank you for your answers </br></br>
        </div>
   


  <?php include 'client/views/form.html' ?>

   <br/>

   <div id="results" class="error">

  
   </div>
</body>
</html>