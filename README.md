# ADM Test task documentation
A formulary with questions in a single page

# Setup

1) Download and install the XAMPP release 7.2.27 package from https://www.apachefriends.org/download.html

3) Put the folder or create a symbolic link to adm_task into the XAMPP/htdocs

4) Import the DB 'adm.sql' into MariaDB http://localhost/phpmyadmin

5) Go to questionary in http://localhost/adm_task/

# Troubleshooting
