function isValid(currentAnswer) {
// Fuction to  validate actual answer
  let result = true;
  $('#results').html('');

  const currentAnswerVal = currentAnswer.val();
  if (!currentAnswerVal && currentAnswerVal.length === 0) {
    $('#results').html('ANSWER IS REQUIRED!!');
    result = false;
  }

  const currentAnswerType = currentAnswer[0].type;
  if (currentAnswerType === 'email') {
    if (!/\S+@\S+\.\S+/.test(currentAnswerVal)) {
      $('#results').html('INVALID EMAIL!!');
      result = false;
    }
  }
  return result;
}

function show(question) {
// Fuction to show the question formQuestions
  question.addClass('show');
  question.removeClass('hidden');
}

function hide(question) {
// Fuction to hidde the question formQuestions
  question.addClass('hidden');
  question.removeClass('show');
}

function next() {
// Main fuction from formQuestions submit
  const currentQuestion = $('#formQuestions div.show');
  const currentAnswer = currentQuestion.find('input');

  if (!isValid(currentAnswer)) {
    return;
  }

  const currentQuestionId = $('#formQuestions .show input')[0].id;
  const nextQuestionId = Number(currentQuestionId) + 1;
  const nextQuestion = $(`#formQuestions #Q${nextQuestionId}`);


  const summary = $('#formQuestions #summary');
  if (nextQuestion[0]) {
    // Show next question
    hide(currentQuestion);
    show(nextQuestion);
  } else if (summary.hasClass('hidden')) {
    // Show summary question
    $('#formQuestions').removeClass('show');
    $('#formQuestions').addClass('hidden');
    $('#formRegister').removeClass('hidden');
    $('#formRegister').addClass('show');
    hide(currentQuestion);
    show(summary);
    // $('#formQuestions #submit')[0].value = 'CHECK ANSWERS';
  } else {
    // $('#formQuestions #submit')[0].value = 'NEXT';
    hide(summary);
  }
}
function submitRegister() {
  // Main fuction from formRegister submit
  const registerQuestions = $('#formRegister');
  const registerAnswers = registerQuestions.find('input');
  $('#formRegister').removeClass('show');

  if (!registerAnswers[0].value || !registerAnswers[1].value || !registerAnswers[2].value) {
    $('#results').html('Please check your data!!');
    return;
  }
  $('#formRegister').addClass('hidden');
  $('#title').addClass('hidden');
  // let submitButton = $('#submitButton').val();
  // let submit = $('#submit').val();
  const r1 = $('#1').val();
  const r2 = $('#2').val();
  const r3 = $('#3').val();
  const r4 = $('#4').val();
  const r11 = $('#11').val();
  const r12 = $('#12').val();
  const r13 = $('#13').val();

  $.post('server/submit.php', { r1, r2, r3, r4, r11, r12, r13 },
    (data) => {
      $('#results').html(data);
      $('#msg').removeClass('hidden');
      $('#msg').addClass('show');
    });
}